# A Csharp sample app for the Hydrogrid API

A minimal example app showing the usage of hydrogrid-api-csharp-client to consume the [HYDROGRID](https://hydrogrid.eu) API.


Code is provided as it without any warranty. 

Author [HYDROGRID GmbH](https://hydrogrid.eu) (c) 2021

## Tested with 

- .NET 5.0 or later
- hydrogrid-api-csharp-client v1


## Dependencies

- [RestSharp](https://www.nuget.org/packages/RestSharp) - 106.11.7 or later
- [Json.NET](https://www.nuget.org/packages/Newtonsoft.Json/) - 12.0.3 or later
- [JsonSubTypes](https://www.nuget.org/packages/JsonSubTypes/) - 1.2.0 or later

The DLLs included in the package may not be the latest version. We recommend using [NuGet](https://docs.nuget.org/consume/installing-nuget) to obtain the latest version of the packages:

```
Install-Package RestSharp
Install-Package Newtonsoft.Json
Install-Package JsonSubTypes
```

```
dotnet add package RestSharp
dotnet add package Newtonsoft.Json
dotnet add package JsonSubTypes
```

## hydrogrid-api-csharp-client
The DLL of the hydrogrid-api-csharp-client is included in /libs in this sample app. 

For trouble shooting re-build the hydrogrid-api-csharp-client and copy IO.Swagger.dll into /libs. 


## Run the sample app (Program.cs)
- Add your credentials (email, secret password) to obtain the JWT token from HYDROGRID's API
- Add your Plant's ID
- Add your control unit identifier

- execute [dotnet run]



