﻿using System.Collections.Generic;
using System.Diagnostics;
using IO.Swagger.Api;
using IO.Swagger.Client;
using IO.Swagger.Model;
using System;
using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;

namespace myApp
{
    public class ApiPowerSubmitActiveGrossActualExample
    {
        public static void Main()
        {
            Configuration.Default.BasePath = "https://api.hydrogrid.eu/v1";
            // Configure HTTP basic authorization: bearerAuth
            Configuration.Default.Username = "your@email.com";
            Configuration.Default.Password = "YourPassword";


            var authApiInstance = new AuthenticationApi(Configuration.Default);

            try
            {
                // Get JWT
                InlineResponse200 result = authApiInstance.ApiAuthGetAccessToken();
                Configuration.Default.AccessToken = result.AccessToken;
            }
            catch (ApiException e)
            {
                Console.WriteLine("Exception when calling AuthenticationApi.ApiAuthGetAccessToken: " + e.Message );
                Console.WriteLine("Status Code: "+ e.ErrorCode);
                Console.WriteLine(e.StackTrace);
            }

            var powerApiInstance = new PowerApi(Configuration.Default);
            var turbineId = "TURBINE ID";  // string | The id of the turbine
            var timeseries = new List<MWTimeseriesElem>();
            timeseries.Add(new MWTimeseriesElem(1614851363000, 42));
            timeseries.Add(new MWTimeseriesElem(1614854963000, 42));
            timeseries.Add(new MWTimeseriesElem(1614858564000, 42));
            var powerSchedule = new PowerScheduleInner(turbineId, timeseries);
            var plantId = "PLANT ID";  // string | The id of the plant
            var requestBody = new List<PowerScheduleInner>(); // List<Object> | Timeseries to submit
            requestBody.Add(powerSchedule);
            try
            {
                // Submit power timeseries for a whole plant
                ApiResponse<Object> res = powerApiInstance.ApiPowerSubmitActiveGrossActualWithHttpInfo(requestBody, plantId);
                Console.WriteLine(res.StatusCode);
                InlineResponse2002 res1 = powerApiInstance.ApiPowerRequestActiveGrossActual(plantId, 1614850000000, 1614859000000);
                Console.WriteLine(res1.Data.ToJson());
            }
            catch (ApiException e)
            {
                Console.WriteLine("Exception when calling PowerApi.ApiPowerSubmitActiveGrossActual: " + e.Message );
                Console.WriteLine("Status Code: "+ e.ErrorCode);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
